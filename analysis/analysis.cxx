#define analysis_cxx

// C++
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <stdlib.h>

// ROOT
#include <TROOT.h>
#include <TSystem.h>
#include <TApplication.h>
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"

#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

// USER DEFINITIONS
#include "analysis.h"

using namespace std;

int main(int argc, char **argv) {
  gROOT->SetBatch(true);

  TApplication theApp("App", &argc, argv);
  gApplication->Init();

  argc=theApp.Argc();
  argv=theApp.Argv();

  TChain* fChain = new TChain("physics");
  fChain->Add("data.root");
  std::string fout = "result.root";

  analysis * exe = new analysis(fChain);
  exe->Loop( fout );
  return EXIT_SUCCESS;
}

void analysis::Loop( string fout )
{
   if (fChain == 0) return;

   TObjArray * Hlist = new TObjArray(0);
   TH1D *h_pt = new TH1D("pt","pt", 600, 0., 60.) ;
   TH1D *h_ptL1 = new TH1D("ptL1","ptL1", 600, 0., 60.) ;
   Hlist->Add(h_pt);
   Hlist->Add(h_ptL1);
   
   Long64_t nentries = fChain->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;

   Double_t pt_tmp=0;

   const Double_t CloseR = 0.4;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      // main analysis part
      // Filling histograms

      for (int mu_it = 0; mu_it < mu_pt->size(); ++mu_it){
        // muon pT in GeV
        pt_tmp = mu_pt->at(mu_it)*0.001;
        cout << "muon pt: "<< pt_tmp << endl;
        h_pt->Fill( pt_tmp );
        for (int L1_it = 0; L1_it < trig_L1_mu_eta->size(); ++L1_it){
          if ( sqrt ((trig_L1_mu_eta->at(L1_it)-mu_eta->at(mu_it))*(trig_L1_mu_eta->at(L1_it)-mu_eta->at(mu_it))
                     +(trig_L1_mu_phi->at(L1_it)-mu_phi->at(mu_it))*(trig_L1_mu_phi->at(L1_it)-mu_phi->at(mu_it))) < CloseR ){
            cout << "L1 trigger fired !"<< endl;
            h_ptL1->Fill( pt_tmp );
            break;
          }
        }
      }
   }
   h_ptL1->Divide(h_pt); 
   TFile *outputfile = new TFile(fout.c_str() , "recreate");
   Hlist->Write();
   outputfile->Close();
}
